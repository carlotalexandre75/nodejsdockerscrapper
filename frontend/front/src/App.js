import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route} from "react-router-dom";

import Navbar from "./components/navbar.component"
import ArticleProcessedLists from "./components/articlesProcessed-list.component";
import EditArticleProcessed from "./components/edit-articleProcessed.component";
import CreateArticleProcessed from "./components/create-articlesProcessed.component";
import ArticleLists from "./components/articles-list.component";
import EditArticle from "./components/edit-article.component";
import CreateArticle from "./components/create-articles.component";

function App() {
  return (
    <Router>
      <div className="container">
      <Navbar />
      <br/>
      <Route path="/article/" component={ArticleLists} />
      <Route path="/article/edit/:id" component={EditArticle} />
      <Route path="/article/create/" component={CreateArticle} />
      <Route path="/articleProcessed/" exact component={ArticleProcessedLists} />
      <Route path="/articleProcessed/edit/:id" component={EditArticleProcessed} />
      <Route path="/articleProcessed/create/" component={CreateArticleProcessed} />
      </div>
    </Router>
  );
}

export default App;
