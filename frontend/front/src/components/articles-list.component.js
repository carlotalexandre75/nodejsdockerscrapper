import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Article = props => (
  <tr>
    <td>{props.article.titre}</td>
    <td>{props.article.pubDate}</td>
    <td>{props.article.description}</td>
    <td>{props.article.lien}</td>
    <td>{props.article.longueur}</td>
    <td>{props.article.img_nbr}</td>
    <td>{props.article.isProcessed}</td>
    <td>
    <Link to={"/article/edit/"+props.article._id}>edit</Link> | <a  onClick={() => { props.deleteArticle(props.article._id) }}>delete</a>
    </td>
  </tr>
);

export default class ArticleList extends Component {
  constructor(props) {
    super(props);

    this.deleteArticle = this.deleteArticle.bind(this);

    this.state = {articles: []};
  }

  componentDidMount() {
    axios.get('http://localhost:5000/article/')
      .then(response => {
        this.setState({ articles: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deleteArticle(id) {
    axios.delete('http://localhost:5000/article/'+id)
      .then(response => { console.log(response.data)});

    this.setState({
      articles: this.state.articles.filter(el => el._id !== id)
    })
  }

  articleList() {
    return this.state.articles.map(currentArticle => {
      return <Article article={currentArticle} deleteArticle={this.deleteArticle} key={currentArticle._id}/>;
    })
  }

  render() {
    return (
      <div>
        <h3>Logged Article </h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>Titre</th>
              <th>Description</th>
              <th>Date de parution</th>
              <th>Lien</th>
              <th>Longueur</th>
              <th>URL de l'image</th>
            </tr>
          </thead>
          <tbody>
            { this.articleList() }
          </tbody>
        </table>
      </div>
    )
  }
}