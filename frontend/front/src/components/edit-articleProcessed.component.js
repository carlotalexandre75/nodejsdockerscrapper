import React, { Component } from 'react';
import axios from 'axios';

export default class EditArticleProcessed extends Component {
  constructor(props) {
    super(props);

    this.onChangeNbrMotTitre = this.onChangeNbrMotTitre.bind(this);
    this.onChangeNbrMotDesc = this.onChangeNbrMotDesc.bind(this);
    this.onChangeIsSun = this.onChangeIsSun.bind(this);
    this.onChangeIsSat = this.onChangeIsSat.bind(this);
    this.onChangeIsFri = this.onChangeIsFri.bind(this);
    this.onChangeIsThu = this.onChangeIsThu.bind(this);
    this.onChangeIsWen = this.onChangeIsWen.bind(this);
    this.onChangeIsTue = this.onChangeIsTue.bind(this);
    this.onChangeIsMon = this.onChangeIsMon.bind(this);
    this.onChangeIsWeekend = this.onChangeIsWeekend.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      nbrMotTitre: '',
      nbrMotDesc: '',
      isMon: '',
      isTue: '',
      isWen: '',
      isThu: '',
      isFri: '',
      isSat: '',
      isSun: '',
      isWeekend: ''
    }
  }

  componentDidMount() {
    axios.get('http://localhost:5000/processedArticle/'+this.props.match.params.id)
      .then(response => {
        this.setState({
          nbrMotTitre: response.data.nbrMotTitre,
          nbrMotDesc: response.data.nbrMotDesc,
          isMon: response.data.isMon,
          isTue: response.data.isTue,
          isWen: response.data.isWen,
          isThu: response.data.isThu,
          isFri: response.data.isFri,
          isSat: response.data.isSat,
          isSun: response.data.isSun,
          isWeekend: response.data.isWeekend
        })   
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  onChangeNbrMotTitre(e) {
    this.setState({
      nbrMotTitre: e.target.value
    })
  }

  onChangeNbrMotDesc(e) {
    this.setState({
      nbMotDesc: e.target.value
    })
  }

  onChangeIsFri(e) {
    this.setState({
      isFri: e.target.value
    })
  }

  onChangeIsMon(e) {
    this.setState({
      isMon: e.target.value
    })
  }

  onChangeIsTue(e) {
    this.setState({
      isTue: e.target.value
    })
  }

  onChangeIsThu(e) {
    this.setState({
      isThu: e.target.value
    })
  }

  onChangeIsWen(e) {
    this.setState({
      isWen: e.target.value
    })
  }

  onChangeIsSat(e) {
    this.setState({
      isSat: e.target.value
    })
  }

  onChangeIsSun(e) {
    this.setState({
      isSun: e.target.value
    })
  }

  onChangeIsWeekend(e) {
    this.setState({
      isWeekend: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();

    const articleProcessed = {
      nbrMotTitre: this.state.nbrMotTitre,
      nbrMotDesc: this.state.nbrMotDesc,
      isMon: this.state.isMon,
      isTue: this.state.isTue,
      isWen: this.state.isWen,
      isThu: this.state.isThu,
      isFri: this.state.isFri,
      isSat: this.state.isSat,
      isSun: this.state.isSun,
      isWeekend: this.state.isWeekend
    };

    console.log(articleProcessed);

    axios.post('http://localhost:5000/processedArticle/update/' + this.props.match.params.id, articleProcessed)
      .then(res => console.log(res.data));

    // this.setState({
    //   nbrMotTitre: '',
    //   nbrMotDesc: '',
    //   isMon: '',
    //   isTue: '',
    //   isWen: '',
    //   isThu: '',
    //   isFri: '',
    //   isSat: '',
    //   isSun: '',
    //   isWeekend: ''
    //     }
    // );

    window.location = '/articleProcessed/';
  }

  render() {
    return (
        <div>
          <h3>Edit Article Processed</h3>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label>Nombre de mots du titre: </label>
              <input  type="text"
                      required
                      className="form-control"
                      value={this.state.nbrMotTitre}
                      onChange={this.onChangeNbrMotTitre}
              />
            </div>
            <div className="form-group">
              <label>Nombre de mots de la description: </label>
              <input  type="text"
                      required
                      className="form-control"
                      value={this.state.nbrMotDesc}
                      onChange={this.onChangeNbrMotDesc}
              />
            </div>
            <div className="form-group">
              <label>Paru le weekend: </label>
              <input
                  type="text"
                  className="form-control"
                  value={this.state.isWeekend}
                  onChange={this.onChangeIsWeekend}
              />
            </div>
            <div className="form-group">
              <label>Paru le lundi: </label>
              <input  type="text"
                      required
                      className="form-control"
                      value={this.state.isMon}
                      onChange={this.onChangeIsMon}
              />
            </div>
            <div className="form-group">
              <label>Paru le mardi: </label>
              <input  type="text"
                      required
                      className="form-control"
                      value={this.state.isTue}
                      onChange={this.onChangeIsTue}
              />
            </div>
            <div className="form-group">
              <label>Paru le mercredi: </label>
              <input  type="text"
                      required
                      className="form-control"
                      value={this.state.isWen}
                      onChange={this.onChangeIsWen}
              />
            </div>
            <div className="form-group">
              <label>Paru le jeudi: </label>
              <input  type="text"
                      required
                      className="form-control"
                      value={this.state.isThu}
                      onChange={this.onChangeIsThu}
              />
            </div>
            <div className="form-group">
              <label>Paru le vendredi: </label>
              <input  type="text"
                      required
                      className="form-control"
                      value={this.state.isFri}
                      onChange={this.onChangeIsFri}
              />
            </div>
            <div className="form-group">
              <label>Paru le samedi: </label>
              <input  type="text"
                      required
                      className="form-control"
                      value={this.state.isSat}
                      onChange={this.onChangeIsSat}
              />
            </div>
            <div className="form-group">
              <label>Paru le dimanche: </label>
              <input  type="text"
                      required
                      className="form-control"
                      value={this.state.isSun}
                      onChange={this.onChangeIsSun}
              />
            </div>
            <div className="form-group">
              <input type="submit" value="Modify Article Processed" className="btn btn-primary" />
            </div>
          </form>
        </div>
    )
  }
}