import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const ArticleProcessed = props => (
  <tr>
    <td>{props.articleProcessed.NbrMotTitre}</td>
    <td>{props.articleProcessed.NbrMotDesc}</td>
    <td>{props.articleProcessed.IsWeekend.toString()}</td>
    <td>{props.articleProcessed.IsMon.toString()}</td>
    <td>{props.articleProcessed.IsTue.toString()}</td>
    <td>{props.articleProcessed.IsWen.toString()}</td>
    <td>{props.articleProcessed.IsThu.toString()}</td>
    <td>{props.articleProcessed.IsFri.toString()}</td>
    <td>{props.articleProcessed.IsSat.toString()}</td>
    <td>{props.articleProcessed.IsSun.toString()}</td>
    <td>
      <Link to={"/articleProcessed/edit/"+props.articleProcessed._id}>edit</Link> | <a  onClick={() => { props.deleteArticleProcessed(props.articleProcessed._id) }}>delete</a>
    </td>
  </tr>
);

export default class ArticleProcessedList extends Component {
  constructor(props) {
    super(props);

    this.deleteArticleProcessed = this.deleteArticleProcessed.bind(this);

    this.state = {articlesProcessed: []};
  }

  componentDidMount() {
    axios.get('http://localhost:5000/processedArticle/')
      .then(response => {
        this.setState({ articlesProcessed: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deleteArticleProcessed(id) {
    axios.delete('http://localhost:5000/processedArticle/'+id)
      .then(response => { console.log(response.data)});

    this.setState({
      articlesProcessed: this.state.articlesProcessed.filter(el => el._id !== id)
    })
  }

  articleList() {
    return this.state.articlesProcessed.map(currentArticle => {
      return <ArticleProcessed articleProcessed={currentArticle} deleteArticleProcessed={this.deleteArticleProcessed} key={currentArticle._id}/>;
    })
  }

  render() {
    return (
      <div>
        <h3>Logged Article Processed</h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>Nombre de mots du titre</th>
              <th>Nombre de mots de la description</th>
              <th>Paru un week end</th>
              <th>Paru un lundi</th>
              <th>Paru un mardi</th>
              <th>Paru un mercredi</th>
              <th>Paru un jeudi</th>
              <th>Paru un vendredi</th>
              <th>Paru un samedi</th>
              <th>Paru un dimanche</th>
            </tr>
          </thead>
          <tbody>
            { this.articleList() }
          </tbody>
        </table>
      </div>
    )
  }
}