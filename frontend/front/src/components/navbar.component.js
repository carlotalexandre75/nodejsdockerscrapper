import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {

  render() {
    return (
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
        <Link to="/" className="navbar-brand">News Scrapper</Link>
        <div className="collpase navbar-collapse">
        <ul className="navbar-nav mr-auto">
          <li className="navbar-item">
              <Link to="/articleProcessed/" className="nav-link">Articles Processed</Link>
          </li>
          <li className="navbar-item">
              <Link to="/articleProcessed/create" className="nav-link">Add Article Processed</Link>
          </li>
          <li className="navbar-item">
              <Link to="/article/" className="nav-link">Articles</Link>
          </li>
          <li className="navbar-item">
              <Link to="/article/create" className="nav-link">Add Article 4 Us 2 Process</Link>
          </li>
        </ul>
        </div>
      </nav>
    );
  }
}