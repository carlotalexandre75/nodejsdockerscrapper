import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class CreateArticle extends Component {
  constructor(props) {
    super(props);

    this.onChangeTitre = this.onChangeTitre.bind(this);
    this.onChangePubDate = this.onChangePubDate.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeLien = this.onChangeLien.bind(this);
    this.onChangeLongueur = this.onChangeLongueur.bind(this);
    this.onChangeImg_nbr = this.onChangeImg_nbr.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      titre: '',
      pubDate: '',
      description: '',
      lien:'',
      longueur:'',
      img_nbr:'',
      isProcessed: false
    }
  }

  onChangeTitre(e) {
    this.setState({
      titre: e.target.value
    });
  }

  onChangePubDate(e) {
    this.setState({
      pubDate: e
    })
  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    })
  }

  onChangeLien(e) {
    this.setState({
      lien: e.target.value
    })
  }

  onChangeLongueur(e) {
    this.setState({
      longueur: e.target.value
    })
  }
  onChangeImg_nbr(e) {
    this.setState({
      img_nbr: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();

    const article_info = {
      titre: this.state.titre,
      pubDate: this.state.pubDate,
      description: this.state.description,
      lien: this.state.lien,
      longueur: this.state.longueur,
      img_nbr: this.state.img_nbr,
      isProcessed: this.state.isProcessed
    };

    console.log(article_info);

    axios.post('http://localhost:5000/article/add', article_info)
      .then(res => console.log(res.data));

    this.setState({
      titre: '',
      pubDate: '',
      description: '',
      lien:'',
      longueur:'',
      img_nbr:'',
      isProcessed: false
    });

    window.location = '/article/';
  }

  render() {
    return (
      <div>
        <h3>Add Article</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Titre: </label>
            <input  type="text"
                    required
                    className="form-control"
                    value={this.state.titre}
                    onChange={this.onChangeTitre}
            />
          </div>
          <div className="form-group">
            <label>Date de publication: </label>
            <div>
              <DatePicker
                  selected={this.state.pubDate}
                  onChange={this.onChangePubDate}
              />
            </div>
          </div>
          <div className="form-group">
            <label>Description: </label>
            <input  type="text"
                    required
                    className="form-control"
                    value={this.state.description}
                    onChange={this.onChangeDescription}
            />
          </div>
          <div className="form-group">
            <label>Lien: </label>
            <input  type="text"
                    required
                    className="form-control"
                    value={this.state.lien}
                    onChange={this.onChangeLien}
            />
          </div>
          <div className="form-group">
            <label>Longueur: </label>
            <input  type="text"
                    required
                    className="form-control"
                    value={this.state.longueur}
                    onChange={this.onChangeLongueur}
            />
          </div>
          <div className="form-group">
            <label>URL d'une image correspondant a l'article: </label>
            <input  type="text"
                    required
                    className="form-control"
                    value={this.state.img_nbr}
                    onChange={this.onChangeImg_nbr}
            />
          </div>
          <div className="form-group">
            <input type="submit" value="Create Article" className="btn btn-primary" />
          </div>
        </form>
      </div>
    )
  }
}