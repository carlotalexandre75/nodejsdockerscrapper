# Informations de lancement du projet

##Instruction pour lancer le code :

<p> il faut lancer un terminal a l'endroit ou est le fichier root ou il y a le docker compose et écrire ces 2 lignes de commande :
Sudo docker-compose build
Sudo docker-compose up

Pour faire des requêtes, il est conseillé d'utilisé Postman : voici un exemples de requêtes pour tester le code
1) il faut run le server.js pour run le server, ne pas oublier de lancer mongoDB aussi 

2) Une fois sur postman on peut se mettre sur http://localhost:5000/article/add pour ajouter un article, le body doit être sous forme JSON, la requête est de type POST :
{
	"titre": "ceci est un exemple de titre3",
	"pubDate": "2019-12-29",
	"description": "ceci est une description3",
	"lien": "http://ouilavie3.com",
	"longueur": 12,
	"img_nbr" : "Nous me suis trompé ca devait être une url 4",
	"IsProcessed": true
}
3) Ensuite on peut récupérer ces infos et les traiter avec cette requête : 

http://localhost:5000/processedArticle/save/dataprocessing
C'est une requête GET 

4) Enfin pour visualiser les info traités il faut de nouveau faire une requête GET :
http://localhost:5000/processedArticle/
</p>
