const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// user (no with spaces) + timestamp
const Articles_Infos_Schema = new Schema({
  titre: {
    type: String,
    required: true
  },
  pubDate: {
    type : String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  lien:{
    type: String,
    required: true
  },
  longueur:{
    type: Number,
    required: true
  },
  img_nbr:{
    type: String,
    required: true
  },
  IsProcessed:{
    type: Boolean,
    required: true,
    default: false
  }
}, {
  timestamps: true,
});

// name + schema
const Articles_Infos = mongoose.model('Articles_Infos', Articles_Infos_Schema);

module.exports = Articles_Infos;