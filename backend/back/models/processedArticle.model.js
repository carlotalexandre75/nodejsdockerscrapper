const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ArticleProcessed = new Schema({
  NbrMotTitre: { type: Number, required: true },
  NbrMotDesc: { type: Number, required: true },
  IsWeekend: { type: Boolean, required: true },
  IsMon: { type: Boolean, required: true },
  IsTue: { type: Boolean, required: true },
  IsWen: { type: Boolean, required: true },
  IsThu: { type: Boolean, required: true },
  IsFri: { type: Boolean, required: true },
  IsSat: { type: Boolean, required: true },
  IsSun: { type: Boolean, required: true }
}, {
  timestamps: true,
});

const ArticlePro = mongoose.model('ArticleProcessed', ArticleProcessed);

module.exports = ArticlePro;