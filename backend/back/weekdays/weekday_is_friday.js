
const is_friday = (day) => {
    return day.substring(0,3).toLowerCase() === "fri" ? true : false;
};

module.exports = {is_friday};