
function is_weekend(day){
    return day.substring(0,3).toLowerCase() === "sat" ? true
        : day.substring(0,3).toLowerCase() === "sun" ? true
            : false
}

module.exports = {is_weekend};