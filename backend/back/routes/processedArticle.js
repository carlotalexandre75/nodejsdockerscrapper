const router = require('express').Router();
let ArticleProcessed = require('../models/processedArticle.model');
let ArticleInfo = require('../models/article.model')

const mon = require('../weekdays/weekday_is_monday');
const tue = require('../weekdays/weekday_is_tuesday');
const wed = require('../weekdays/weekday_is_wednesday');
const thu = require('../weekdays/weekday_is_thursday');
const fri = require('../weekdays/weekday_is_friday');
const sat = require('../weekdays/weekday_is_saturday');
const sun = require('../weekdays/weekday_is_sunday');
const wee = require('../weekdays/is_weekend');
const NbrWord= require ('../WordCount/CountWord')

router.route('/').get((req, res) => {
    ArticleProcessed.find()
        .then(exercises => res.json(exercises))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
    // get params from req body
    const NbrMotTitre = req.body.NbrMotTitre;
    const NbrMotDesc = req.body.NbrMotDesc;
    const IsWeekend = req.body.IsWeekend;
    const IsMon = req.body.IsMon;
    const IsTue = req.body.IsTue;
    const IsWen = req.body.IsWen;
    const IsThu = req.body.IsThu;
    const IsFri = req.body.IsFri;
    const IsSat = req.body.IsSat;
    const IsSun = req.body.IsSun;


    const newArticleProcessed = new ArticleProcessed({
        NbrMotTitre,
        NbrMotDesc,
        IsWeekend,
        IsMon,
        IsTue,
        IsWen,
        IsThu,
        IsFri,
        IsSat,
        IsSun
    });

    newArticleProcessed.save()
        .then(() => res.json('Processed article added!'))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/save/dataprocessing').get((req, res) => {
    // Need to add loop on all articles
    ArticleInfo.find()
        .then(article => {
            const NbrMotTitre =  NbrWord.WordCount(article[0].titre);
            const NbrMotDesc = NbrWord.WordCount(article[0].description);


            //const NbrMotTitre = NbrWord.WordCountArticle(NbrWordTitle);
            //const NbrMotDesc = NbrWord.WordCountArticle(NbrWordDesc)
            const day = article[0].pubDate;
            //const NbrMotTitre = 9;
            //const NbrMotDesc = 9;
            const IsMon = mon.is_monday(day);
            const IsTue = tue.is_tuesday(day);
            const IsWen = wed.is_wednesday(day);
            const IsThu = thu.is_thursday(day);
            const IsFri = fri.is_friday(day);
            const IsSat = sat.is_saturday(day);
            const IsSun = sun.is_sunday(day);
            const IsWeekend = wee.is_weekend(day);

            const processedArticle = new ArticleProcessed({
                NbrMotTitre,
                NbrMotDesc,
                IsWeekend,
                IsMon,
                IsTue,
                IsWen,
                IsThu,
                IsFri,
                IsSat,
                IsSun,
            });

            processedArticle.save()
                .then(() => res.json('Processed Article added!'))
                .catch(err => res.status(400).json('Error: ' + err));
        })
});

router.route('/:id').get((req, res) => {
    // get id from url
    ArticleProcessed.findById(req.params.id)
        .then(article => res.json(article))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
    ArticleProcessed.findByIdAndDelete(req.params.id)
        .then(() => res.json('Processed article deleted.'))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').post((req, res) => {
    ArticleProcessed.findById(req.params.id)
        .then(article => {
            article.NbrMotTitre = Number(req.body.NbrMotTitre);
            article.NbrMotDesc = Number(req.body.NbrMotDesc);
            article.IsWeekend = req.body.IsWeekend;
            article.IsMon = req.body.IsMon;
            article.IsTue = req.body.IsTue;
            article.IsWen = req.body.IsWen;
            article.IsThu = req.body.IsThu;
            article.IsFri = req.body.IsFri;
            article.IsSat = req.body.IsSat;
            article.IsSun = req.body.IsSun;

            article.save()
                .then(() => res.json('Processed article updated!'))
                .catch(err => res.status(400).json('Error: ' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;