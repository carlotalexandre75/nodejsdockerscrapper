const router = require('express').Router();
let ArticleInfo = require('../models/article.model');

router.route('/').get((req, res) => {
  ArticleInfo.find()
      .then(users => res.json(users))
      .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const titre = req.body.titre;
  const pubDate = req.body.pubDate;
  const description = req.body.pubDate;
  const lien = req.body.lien;
  const longueur = req.body.longueur;
  const img_nbr = req.body.img_nbr;
  const IsProcessed = req.body.IsProcessed;

  const NewArticlesInfos = new ArticleInfo({
    titre,
    pubDate,
    description,
    lien,
    longueur,
    img_nbr,
    IsProcessed
  });

  NewArticlesInfos.save()
      .then(() => res.json('Article added!'))
      .catch(err => res.status(400).json('Error: ' + err));
});

// router.route('/save/dataprocessing').get((req, res) => {
//   // Need to add loop on all articles
//   ArticleInfo.find()
//       .then(article => {
//         const NbrMotTitre =  NbrWord.WordCount(article[0].titre);
//         const NbrMotDesc = NbrWord.WordCount(article[0].description);
//
//
//         //const NbrMotTitre = NbrWord.WordCountArticle(NbrWordTitle);
//         //const NbrMotDesc = NbrWord.WordCountArticle(NbrWordDesc)
//         const day = article[0].pubDate;
//         //const NbrMotTitre = 9;
//         //const NbrMotDesc = 9;
//         const IsMon = mon.is_monday(day);
//         const IsTue = tue.is_tuesday(day);
//         const IsWen = wed.is_wednesday(day);
//         const IsThu = thu.is_thursday(day);
//         const IsFri = fri.is_friday(day);
//         const IsSat = sat.is_saturday(day);
//         const IsSun = sun.is_sunday(day);
//         const IsWeekend = wee.is_weekend(day);
//
//         const processedArticle = new ArticleProcessed({
//           NbrMotTitre,
//           NbrMotDesc,
//           IsWeekend,
//           IsMon,
//           IsTue,
//           IsWen,
//           IsThu,
//           IsFri,
//           IsSat,
//           IsSun,
//         });
//
//         processedArticle.save()
//             .then(() => res.json('Processed Article added!'))
//             .catch(err => res.status(400).json('Error: ' + err));
//       })
// });

router.route('/:id').get((req, res) => {
  // get id from url
  ArticleInfo.findById(req.params.id)
      .then(article => res.json(article))
      .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
  ArticleInfo.findByIdAndDelete(req.params.id)
      .then(() => res.json('Article deleted.'))
      .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').post((req, res) => {
  ArticleInfo.findById(req.params.id)
      .then(article => {
        article.titre = req.body.titre;
        article.pubDate = req.body.pubDate;
        article.description = req.body.pubDate;
        article.lien = req.body.lien;
        article.longueur = req.body.longueur;
        article.img_nbr = req.body.img_nbr;
        article.IsProcessed = req.body.IsProcessed;

        article.save()
            .then(() => res.json('Article updated!'))
            .catch(err => res.status(400).json('Error: ' + err));
      })
      .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;