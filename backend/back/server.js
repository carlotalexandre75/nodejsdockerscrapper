const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

// env vars in .env file
require('dotenv').config();

// Create express server
const app = express();
const port = process.env.PORT || 5000;

// Middlerwares (cors and json parser)
app.use(cors());
app.use(express.json());

// DB uri + connection
const uri = 'mongodb://mongodb/myapp';
// flag to deal with updates
mongoose.connect(uri);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
})

const processedArticleRouter = require('./routes/processedArticle');
const articleRouter = require('./routes/article');

app.use('/processedArticle', processedArticleRouter);
app.use('/article', articleRouter);

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});
