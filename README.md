# Rapport de projet
Ce projet a été réalisé par:
- Carlot Alexandre
- Moise Andrei
- Lecoeur Louis 

## Introduction
<p> Notre projet permet de poster un article au format JSON dans base de donnee mongoDB et de le mettre dans une base de
 données mongoDB. Ensuite différentes fonctions sont appliquées sur l'article enrégistré précedemment, ces nouvelles données
  traitee sont sauvegardées egalement, tout ceci via une route dédiée.</p>
    
## Architecture du projet
<p> DataBase : Nous allons utiliser MongoDB pour notre base de données. Il s'agit d'une base NoSQL qui stocke des 
documents divers, sans structure ni schéma fixe. La flexibilité est le principal avantage.</p>

<p> Backend : Pour le backend, nous allons utiliser NodeJS et Express comme serveur. Nous exécuterons une application 
Javascript côté serveur (API) qui sera le middleware entre le client et la base de données.</p>
<p> 
FrontEnd: ReactJS: la bibliothèque Javascript de Facebook pour l'interface utilisateur; Bootstrap 3: Framework HTML, 
CSS et JS pour le développement web
</p>

<p>Nous avons décider de séparer ce Projet en 3 services afin de simplifier l'architecture. </p>

### Back 

---
#### DB
<p> Nous avions d'abord choisi une base Mysql pour stocker nos articles. Malheureusement nous n'arrivions pas a récupérer certains résultats de fonctions, ni d'afficher ces résultats grace a des requetes. Il y avait des problèmes d'asynchronisme, nous avons donc décider d'utiliser MongoDB, plus flexible pour le format des articles, mais aussi bien mieu documente. Nous avons créé 2 tables, Articles et ArticlesProcessed: dans la première on stock les articles que l'on a créé via des routes puis dans la deuxième on stocks les résultats des nos fonctions
<p>


#### Code
<p> 2 routes principales:</p>

- <p> http://localhost:5000/processedArticle/ </p>
- <p> http://localhost:5000/article/ </p>

<p> L'une permettant d'effectuer des taitements sur les articles traites, et l'autre les articles non traite.
 Ensuite des sous routes permettent de voir les articles, d'en ajouter, 
ou de les sauvegarder dans la db. Il également possible de trouver des articles en fonction de leurs id, de les supprimer ou de les mettre a jour.  </p>

<p> Toutes les operations CRUD sont implementes pour chacunes de ces routes.</p>

<p> Les fonctions de traitement de l'article permettent de: </p> 

- <p> compter les mots</p>
- <p> vérifier la date de publication</p>
- <p> Indiquer si l'article a ete traite ou non</p>

<p> Express a servi de base pour la conception notre server. Dans le fichier server.js est etabli l'arboresence de nos 
routes  mais aussi la connection avec la DB. </p>

### Front 

---
<p>
React a ete utilise pour le rendu et Axios pour les requetes vers le back
</p>

## Instructions pour executer le projet :
### Docker
<p> Lancer un terminal, se placer la ou est situe le fichier docker-compose.yaml et entrer ces 2 commandes
"sudo docker-compose build" puis "sudo docker-compose up"

Cela creera 3 conteneur: 1 pour la DB, 1 pour le back, 1 pour le front

Entrez ensuite l'url suivante dans votre navigateur, de preference Chrome (sans addblock) http://localhost:3000/ 

Vous avez desormais acces au front qui va lui meme transferer les requetes au back qui effectuera les requetes vers la base de donnee
</p>

### Run soit back, front, ou DB

---
#### Back
<p> 
Lancer un terminal, se placer dans le dossier back, executer "npm install" puis "node server.js" ou "nodemon server.js" si vous avez installe nodemon. Nodemon permet au server de s'actualiser automatiquement a chaque modification d'un fichier
</p>
<p>
Pour execturer des requêtes, il est necessaire d'utiliser Postman afin de tester les requetes POST : voici un exemples de requêtes pour tester le code

1) Une fois sur postman on peut se mettre sur http://localhost:5000/article/add pour ajouter un article, le body doit être sous forme JSON, la requête est de type POST :
{
	"titre": "ceci est un exemple de titre3",
	"pubDate": "2019-12-29",
	"description": "ceci est une description3",
	"lien": "http://ouilavie3.com",
	"longueur": 12,
	"img_nbr" : "Nous me suis trompé ca devait être une url 4",
	"IsProcessed": true
}
2) Ensuite on peut récupérer ces infos et les traiter avec cette requête : 

http://localhost:5000/processedArticle/save/dataprocessing
C'est une requête GET 

4) Enfin pour visualiser les info traités il faut de nouveau faire une requête GET :
http://localhost:5000/processedArticle/
</p>

#### Front 
<p>
Lancer un terminal, se placer dans le dossier front, executer "npm install" puis "npm start"

Entrez ensuite l'url suivante dans votre navigateur, de preference Chrome (sans addblock) http://localhost:3000/ 
</p>

#### DB 
<p>
CF Doc MongoDB

Ubuntu: sudo service start mongod
</p>

## Docker

<p> Dans le fichier docker compose , nous définissons les conteneurs dont nous avons besoin. Nous définissons la création de 3 conteneurs «mongodb», «backend» et «frontend».

Mongodb: utilisera l'image «mongo» et exposera le port de conteneur 27017 comme port 27017 dans la machine hôte.

Backend: utilisera l'image définie dans le «Dockerfile» qui existe dans le répertoire «./node-backend/» (option de build). Présentera également le port 6200 dans la machine hôte. Le dossier de l'application (défini dans le Dockerfile comme Workdir) sera «./node-backend» sur notre machine (option de volume).

Frontend: utilisera l'image définie dans le «Dockerfile» qui existe dans le répertoire «./node-frontend/» (option de build). Présentera également le port 3000 dans la machine hôte. Le dossier de l'application (défini dans le Dockerfile comme Workdir) sera «./node-frontend» sur notre machine (option de volume).

L'option «depend_on» indique que le conteneur sera chargé après ce service. 
L'exécution sera comme ceci:

Mongodb-> Backend-> Frontend
</p>

